# Chat Proxy

Proxy for chat related function:

1. Connect Facebook Messenger to storage (Airtable for now)
2. Connect `web-ui` to reporter's messenger via Facebook Page

## Environment Variable

Use `.env` or env var to setup the following variable:

- Database
  - `AIRTABLE_KEY`: Airtable API key
  - `AIRTABLE_BASE_ID`: Airtable base to use
- Chat bot
  - `MESSENGER_PAGE_ID`: Facebook Page to use
  - `MESSENGER_ACCESS_TOKEN`: Page access token
  - `MESSENGER_APP_ID` & `MESSENGER_APP_SECRET`: Facebook App ID
  - `MESSENGER_VERIFY_TOKEN`: Chatbot verify token
- UI Login
  - `CLIENT_ENDPOINTS`: comma separated list of login domain (for CORS)
  - `API_TOKEN`: token for acceptable client connection
  - `UI_ENDPOINT`: UI endpoint for URL generation
- Option log
  - `SENTRY_DSN`: dsn of sentry

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload and ngrok
# need to update
$ npm run dev

# production
$ npm start
```
