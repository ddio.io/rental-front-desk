const Airtable = require('airtable')

let issueTable = null

class UserIssueMap {
  // map of user and issue to speedup Airtable super slow API..
  constructor () {
    this.userMap = {
      // <user-id>: <issue>
    }
    this.issueMap = {
      // <issue-id>: <user-id>
    }

    // reset every 30 min to avoid memory leak XD
    this.resetPeriod = 30 * 60 * 1000
    setInterval(() => {
      this.reset()
    }, this.resetPeriod)
  }

  reg (userId, issue) {
    this.userMap[userId] = issue
    this.issueMap[issue.id] = userId
  }

  userOfIssue (issueId) {
    return this.issueMap[issueId]
  }

  issueOfUser (userId) {
    return this.userMap[userId]
  }

  issue (issueId) {
    if (this.issueMap[issueId]) {
      return this.userMap[this.issueMap[issueId]]
    }
    return undefined
  }

  updateIssue (issue) {
    const targetUserId = this.issueMap[issue.id]
    if (targetUserId) {
      this.userMap[targetUserId] = issue
      return true
    }
    return false
  }

  reset () {
    this.userMap = {}
    this.issueMap = {}
  }
}

const userIssueMap = new UserIssueMap()

function initIssueTable () {
  issueTable = new Airtable({
    apiKey: process.env.AIRTABLE_KEY
  }).base(
    process.env.AIRTABLE_BASE_ID
  )
}

function getTable (base = '通報案件') {
  if (!issueTable) {
    initIssueTable()
  }
  return issueTable(base)
}

function pushChatLog (issue, chat, isFromClient) {
  const newLog = issue.fields.聯絡紀錄 ? JSON.parse(issue.fields.聯絡紀錄) : []

  newLog.push({
    time: (new Date()).toISOString(),
    isFromClient,
    payload: chat
  })

  return newLog
}

const api = {
  async getIssue (issueId) {
    const result = await getTable().find(issueId)
    return result
  },
  // return issue if it's connected to a user
  async getIssueWithUser (issueId) {
    let userId = userIssueMap.userOfIssue(issueId)
    if (userId) {
      return userIssueMap.issue(issueId)
    }
    const issue = await api.getIssue(issueId)
    userId = issue.fields.通報人臉書
    if (userId) {
      userIssueMap.reg(userId, issue)
      return issue
    }
    return undefined
  },
  async getUserLastIssue (userId) {
    if (userIssueMap.issueOfUser(userId)) {
      return userIssueMap.issueOfUser(userId)
    }

    const promise = new Promise((resolve, reject) => {
      const query = getTable().select({
        filterByFormula: `{通報人臉書} = '${userId}'`,
        sort: [
          { field: '最後更新時間', direction: 'desc' }
        ],
        maxRecords: 1
      })
      query.firstPage((err, result) => {
        if (!err) {
          resolve(result)
        } else {
          reject(err)
        }
      })
    })

    const rows = await promise

    if (rows.length) {
      userIssueMap.reg(userId, rows[0])
      return rows[0]
    }
    return undefined
  },
  async regIssue (issueId, userId) {
    let issues = []
    try {
      issues = await getTable().update([{
        id: issueId,
        fields: {
          通報人臉書: userId
        }
      }])
    } catch (e) {
      console.error('Fail to reg issue: ', e)
    }

    if (issues && issues.length) {
      userIssueMap.reg(userId, issues[0])
      return issues[0]
    }
    return undefined
  },
  async pushChat ({ issueId, userId, payload, isFromClient }) {
    let issue = null
    // get issue from either issueId or userId
    if (issueId) {
      issue = userIssueMap.issue(issueId)
      if (!issue) {
        try {
          issue = await getTable().find(issueId)
          userIssueMap.reg(issue.fields.通報人臉書, issue)
        } catch (e) {
          console.error('no such issue:', e)
        }
      }
    } else if (userId) {
      issue = userIssueMap.issueOfUser(userId)
      if (!issue) {
        issue = await api.getUserLastIssue(userId)
      }
    } else {
      throw new Error('Either issueId or userId is required')
    }

    if (!issue) {
      return undefined
    }

    // push log to airtable
    const newLog = pushChatLog(issue, payload, isFromClient)

    let results
    try {
      results = await getTable().update([{
        id: issue.id,
        fields: {
          聯絡紀錄: JSON.stringify(newLog)
        }
      }])
    } catch (e) {
      console.error('Error update chat log: ', e)
    }

    if (!results || !results.length) {
      return undefined
    }

    userIssueMap.updateIssue(results[0])
    return results[0]
  },
  async retrieveLog (issueId) {
    let issue = userIssueMap.issue(issueId)
    if (!issue) {
      try {
        issue = await api.getIssue(issueId)
      } catch (e) {
        console.error('Failed to parse chat log', e)
      }
    }
    if (!issue || !issue.get('通報人臉書')) {
      return null
    }
    if (!issue.fields.聯絡紀錄) {
      return []
    }
    return JSON.parse(issue.fields.聯絡紀錄)
  },
  getLastChat (issue) {
    try {
      const logs = JSON.parse(issue.get('聯絡紀錄'))
      return logs[logs.length - 1]
    } catch (e) {
      console.error('Failed to parse chat log', e)
    }
  }
}

module.exports = api
