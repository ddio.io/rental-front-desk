const uuidv4 = require('uuid/v4')
const airtable = require('./airtable')
const { getClient } = require('bottender')

const messenger = getClient('messenger')
const WS_ID = '_switcher_id'

class LineSwitcher {
  constructor () {
    this.admins = {
      // <websocket-id>: {
      //   ws: <websocket>,
      //   issueId: <issue-id>
      // }
    }
    this.adminBook = {
      // <issue-id>: set(<websocket-id>)
    }
  }

  hookIssue (issueId, ws) {
    if (!ws[WS_ID]) {
      ws[WS_ID] = uuidv4()
    }
    const wsId = ws[WS_ID]
    const adminInfo = this.admins[wsId]
    if (adminInfo && adminInfo.issueId) {
      // only one issue reg at a time :)
      this.unhookIssue(ws)
    }
    this.admins[wsId] = { ws }
    this.admins[wsId].issueId = issueId
    if (!this.adminBook[issueId]) {
      this.adminBook[issueId] = new Set()
    }
    this.adminBook[issueId].add(wsId)
    console.log(`[HOOK] ${Object.keys(this.admins).length}`)
  }

  unhookIssue (ws) {
    const wsId = ws[WS_ID]
    const adminInfo = this.admins[wsId]
    if (!adminInfo || !adminInfo.issueId) {
      return
    }
    delete this.admins[wsId]

    const book = this.adminBook[adminInfo.issueId]
    book.delete(wsId)
    if (book.size === 0) {
      delete this.adminBook[adminInfo.issueId]
    }
    adminInfo.issueId = null
    console.log(`[UNHOOK] ${Object.keys(this.admins).length}`)
  }

  async toMessenger (ws, payload) {
    const adminInfo = this.admins[ws[WS_ID]]
    if (!adminInfo) {
      return
    }
    const issue = await airtable.getIssueWithUser(adminInfo.issueId)
    if (!issue) {
      // nothing todo :)
      return
    }
    const issueRow = await airtable.pushChat({
      issueId: issue.id,
      payload: payload,
      isFromClient: false
    })
    if (typeof payload.value === 'string') {
      messenger.sendText(
        issue.fields.通報人臉書,
        payload.value,
        { tag: 'CONFIRMED_EVENT_UPDATE' }
      )
    } else {
      messenger[payload.method](
        issue.fields.通報人臉書,
        payload.value,
        { tag: 'CONFIRMED_EVENT_UPDATE' }
      )
    }
    return issueRow
  }

  async toAdmin (issueId, payload) {
    const issue = await airtable.pushChat({
      issueId,
      payload,
      isFromClient: true
    })
    const wsSet = this.adminBook[issueId]
    if (!wsSet) {
      // nothing todo :)
      return
    }
    const lastChat = airtable.getLastChat(issue)
    wsSet.forEach(wsId => {
      this.admins[wsId].ws.send(JSON.stringify({
        cmd: 'MESSENGER_MSG',
        payload: lastChat
      }))
    })
    return issue
  }
}

const switcher = new LineSwitcher()

module.exports = switcher
