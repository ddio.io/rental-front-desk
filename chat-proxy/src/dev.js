const ngrok = require('ngrok')
const path = require('path')

const bottenderConfig = require(path.join(__dirname, '../bottender.config'))
const port = Number(process.env.PORT) || 5000;

(async function () {
  const url = await ngrok.connect(port)
  console.log(`[Ngrok] public url: ${url}`)
  Object.keys(bottenderConfig.channels).forEach((channel) => {
    const config = bottenderConfig.channels[channel]
    if (!config.enabled) {
      return
    }
    console.log(`[Ngrok] ${channel} callback: ${url}${config.path}`)
  })
})()
