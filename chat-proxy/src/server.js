// init env config
require('dotenv').config()

const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')

const { bottender } = require('bottender')
const admin = require('./handler/admin')

const requiredEnvs = [
  'CLIENT_ENDPOINTS',
  'API_TOKEN'
]

if (requiredEnvs.some(key => !process.env[key])) {
  throw new Error('Make sure you have all required env var')
}

const app = express()

// setup log
const Sentry = require('@sentry/node')

if (process.env.SENTRY_DSN) {
  Sentry.init({ dsn: process.env.SENTRY_DSN })
  // The request handler must be the first middleware on the app
  app.use(Sentry.Handlers.requestHandler())
}

// setup auth
const corsOptions = {
  origin: process.env.CLIENT_ENDPOINTS.split(',')
}

app.use(cors(corsOptions))

// basic express config
app.use(
  bodyParser.json({
    verify: (req, _, buf) => {
      req.rawBody = buf.toString()
    }
  })
)

// health check
app.get('/api/health', (req, res) => {
  res.json({ ok: true })
})

// admin web socket
admin(app)

// chat bot
const bot = bottender({
  dev: process.env.NODE_ENV !== 'production'
})
const botHandler = bot.getRequestHandler()

app.all('*', (req, res) => {
  return botHandler(req, res)
})

app.use(Sentry.Handlers.errorHandler())

bot.prepare().then(() => {
  const port = Number(process.env.PORT) || 5000

  app.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
