const lineSwitcher = require('../storage/lineSwitcher')
const airtable = require('../storage/airtable')

// ping every 20 sec
const PING_PERIOD = 20 * 1000

function wsSend (ws, payload) {
  ws.send(JSON.stringify(payload))
}

function initWebsocket (app) {
  // eslint-disable-next-line no-unused-vars
  const expws = require('express-ws')(app)

  // expws.getWss().on('connection', function (ws) {
  //   console.log('connection open')
  //   const userNames = Object.values(store.store.users)
  //     .map(u => u.name)
  //     .join(', ')
  //   ws.send(`welcome! now we got users in line: ${userNames}.`)
  //   store.regWs(ws)
  // })

  app.ws('/ws', (ws, req) => {
    if (!req.query.token || req.query.token !== process.env.API_TOKEN) {
      ws.terminate()
      return
    }
    // ensure connection health
    ws._isAlive = true
    ws._pingCallback = setInterval(() => {
      if (!ws._isAlive) {
        ws.terminate()
      } else {
        ws.ping(() => {})
      }
    }, PING_PERIOD)

    ws.on('pong', () => {
      ws._isAlive = true
    })

    ws.on('close', function close () {
      console.log('disconnected')
      clearInterval(ws._pingCallback)
      lineSwitcher.unhookIssue(ws)
    })
    ws.on('message', async function (msg) {
      let payload = {}
      try {
        payload = JSON.parse(msg)
      } catch (e) {
        // not a valid payload
        return
      }
      if (!payload.cmd) {
        return
      }
      console.log(`[SERVER] Got ${payload.cmd}`)
      if (payload.cmd === 'REGISTER') {
        lineSwitcher.hookIssue(payload.issueId, ws)
        const log = await airtable.retrieveLog(payload.issueId)
        wsSend(ws, {
          cmd: 'REGISTER_DONE',
          log: log
        })
      } else if (payload.cmd === 'UNREGISTER') {
        lineSwitcher.unhookIssue(ws)
        wsSend(ws, {
          cmd: 'UNREGISTER_DONE'
        })
      } else if (payload.cmd === 'TO_MESSENGER') {
        const issue = await lineSwitcher.toMessenger(ws, payload)
        const lastChat = airtable.getLastChat(issue)
        wsSend(ws, {
          cmd: 'TO_MESSENGER_DONE',
          payload: lastChat
        })
      }
    })
  })
}

module.exports = initWebsocket
