// module.exports = async function App(context) {
//   await context.sendText('Hello World');
// };
// const store = require('./src/store')
const airtable = require('./src/storage/airtable')
const lineSwitcher = require('./src/storage/lineSwitcher')

module.exports = async function App (context) {
  // const e = context.event
  // console.log('=====', user, e.rawEvent)
  const userId = context.session.user.id
  const ev = context.event
  const issue = await airtable.getUserLastIssue(userId)

  if (ev.isReferral) {
    console.log('[BOT] referral!', ev.ref)
    const issue = await airtable.regIssue(ev.ref, userId)
    if (!issue) {
      console.log(`Invalid issue for reg: ${ev.ref}`)
      return
    }

    const apartment = issue.fields.通報公寓.split(' ')
    const type = issue.fields.通報緣由.split(' ')
    const historyUrl = `${process.env.UI_ENDPOINT}item-state/${ev.ref}`
    const replyZh = [
      '訂閱成功！後續關於',
      apartment[0],
      '的',
      type[0],
      '問題，都會使用 Messenger 聯絡。',
      '若想瀏覽處理紀錄，也可點選下方連結。'
    ].join('')
    const replyEn = [
      'Well done! We will notify you all update about ',
      type.slice(1).join(),
      ' issue in ',
      apartment.slice(1).join(),
      '. For more inquiry, just send us a message here. ',
      'To get completed issue history, please visit the following page.'
    ].join('')
    const msg = `${replyZh}\n\n${replyEn}\n\n${historyUrl}`
    await context.sendText(msg)

    const user = await context.getUserProfile()
    airtable.pushChat({
      issueId: issue.id,
      payload: {
        type: 'text',
        value: msg
      },
      isFromClient: false
    })

    console.log('[BOT] User reg: ', user)
  } else if (issue) {
    // only reply when this user is hook to a issue
    console.log(`[BOT] got issue ${issue.id} from ${userId}`)
    if (ev.isText) {
      lineSwitcher.toAdmin(issue.id, {
        type: 'text',
        value: ev.text
      })
    } else if (ev.isLikeSticker) {
      lineSwitcher.toAdmin(issue.id, {
        type: 'like'
      })
    } else if (ev.isImage) {
      lineSwitcher.toAdmin(issue.id, {
        type: 'image',
        value: ev.attachments
      })
    } else if (ev.isDelivery) {
      // noop
    } else {
      console.error('unhandled data', ev)
    }
  } else {
    console.log('[BOT] non-issue', userId)
  }
}

// to get user name
// curl -X GET
// "https://graph.facebook.com/<PSID>?fields=first_name,last_name,profile_pic&access_token=<PAGE_ACCESS_TOKEN>"
