// import moment from 'moment'
import api from '~/utils/api'

export const adminMixin = {
}

export const issueMixin = {
  methods: {
    zh (field) {
      if (!field) {
        return ''
      }
      return field.split(' ')[0]
    },
    en (field) {
      if (!field) {
        return ''
      }
      return field.split(' ').slice(1).join(' ')
    },
    async sendToMessenger (msg, opts = {}) {
      msg = msg.trim()
      if (!msg && !opts.value) {
        return
      }
      const payload = {
        type: 'text',
        value: msg,
        ...opts
      }
      await api.chat.sendMsg(payload)
    }
  }
}
