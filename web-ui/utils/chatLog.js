class ChatLog {
  constructor (payload) {
    // trust data from server
    Object.keys(payload).forEach((key) => {
      // backward compatibility
      if (key === 'isFromClient') {
        this.source = payload.isFromClient ? 'client' : 'admin'
      } else {
        this[key] = payload[key]
      }
    })
  }

  get isFromClient () {
    return !this.source || this.source === 'client'
  }

  get isFromAdmin () {
    return this.source === 'admin'
  }

  get isFromSystem () {
    return this.source === 'system'
  }
}

export default ChatLog
