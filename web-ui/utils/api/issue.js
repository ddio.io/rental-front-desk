import feathers from './feathers'
import { makeTime } from './utils'

function normalizeRow (row) {
  if (Array.isArray(row)) {
    return row.map(theRow => normalizeRow(theRow))
  } else {
    row = makeTime(row, 'reportedAt', 'atUpdatedAt', 'finishedAt')
    row.state = row.state || '新通報'
    return row
  }
}

export const issueApi = {
  async find (query) {
    const resp = await feathers.service('ticket').find({ query })
    resp.data = normalizeRow(resp.data)
    return resp
  },
  async get (id) {
    const resp = await feathers.service('ticket').get(id)
    return normalizeRow(resp)
  },
  getReport (id) {
    return feathers.service('member-report').get(id)
  },
  async updateState (id, state, msg) {
    const result = await feathers.service('member-report').patch(id, {
      toState: state,
      msg
    })

    if (!result) {
      return null
    }
    return result
  }
}

export default issueApi
