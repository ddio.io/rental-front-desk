import feathers from './feathers'

export const airsyncApi = {
  find (query) {
    return feathers.service('airsync').find({ query })
  },
  sync (service) {
    return feathers.service('airsync').create({ service })
  }
}

export default airsyncApi
