import feathers from './feathers'

const cache = {}
const HOLIDAY_STATE = '下班中'

async function getMacro (service, filter) {
  if (!cache[service]) {
    const list = await feathers.service(service).find({
      query: {
        $limit: 1000
      }
    })
    if (typeof filter === 'function') {
      cache[service] = list.data.filter(filter)
    } else {
      cache[service] = list.data
    }
  }
  return cache[service]
}

export const macroAPI = {
  listCannedMsg () {
    return getMacro('canned-msg', row => !!row.msg)
  },
  async listTicketState () {
    const states = await getMacro('ticket-state')
    return states.filter(state => state.internalState !== HOLIDAY_STATE)
  },
  async getHolidayTime () {
    const states = await getMacro('ticket-state')
    return states.find(state => state.internalState === HOLIDAY_STATE)
  },
  async normalizeState (internalState) {
    const states = await getMacro('ticket-state')
    const theState = states.find(state => state.internalState === internalState)
    if (theState) {
      return internalState
    }
    return states[0].internalState
  }
}

export default macroAPI
