import issue from './issue'
import chat from './chat'
import macro from './macro'
import airsync from './airsync'
import feathers from './feathers'

const api = {
  issue,
  chat,
  macro,
  feathers,
  airsync
}

export default api

export {
  issue,
  chat,
  macro,
  feathers,
  airsync
}
