import moment from 'moment'
const restEndpoint = process.env.REST_ENDPOINT

function composeUrl (resource) {
  return `${restEndpoint}/${resource}/`
}

function makeTime (row, ...fields) {
  fields.push('createdAt', 'updatedAt')
  fields.forEach((field) => {
    if (row[field]) {
      // example: 2020-03-12 08:46:21.000 +00:00
      // this only happened in sqlite...
      const dateStr = row[field].replace(' ', 'T').replace(' ', '')
      row[field] = moment(dateStr)
    }
  })
  return row
}

function makeJson (row, ...fields) {
  fields.forEach((field) => {
    // sqlite return string, however, pgsql return object...
    if (row[field] && typeof row[field] === 'string') {
      try {
        row[field] = JSON.parse(row[field])
      } catch (err) {
        console.error('Invalid json:', row[field], err)
        row[field] = {}
      }
    }
  })
  return row
}

export {
  composeUrl,
  makeTime,
  makeJson
}
