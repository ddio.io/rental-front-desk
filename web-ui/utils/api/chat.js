import feathers from './feathers'

const chatApi = {
  getRaw (id) {
    return feathers.service('chat').get(id)
  },
  find (params) {
    return feathers.service('member-chat').find({ query: params })
  },
  markAsRead (id) {
    return feathers.service('chat').patch(id, {
      isRead: true
    })
  },
  sendMsg ({ channelType = 'facebook', channelId, type = 'text', data }) {
    return feathers.service('member-chat').create({
      channelId,
      channelType,
      type,
      data
    })
  }
}

export default chatApi
