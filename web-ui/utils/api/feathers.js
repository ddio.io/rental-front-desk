import feathers from '@feathersjs/feathers'
import auth from '@feathersjs/authentication-client'
import rest from '@feathersjs/rest-client'
import socketio from '@feathersjs/socketio-client'
import io from 'socket.io-client'
import axios from 'axios'

class Feathers {
  constructor () {
    this.app = feathers()
    this.hasInit = false
  }

  async init (requireLogin = false) {
    if (this.hasInit) {
      return
    }
    if (requireLogin) {
      const socket = io(process.env.API_ENDPOINT)
      this.app.configure(socketio(socket, {
        timeout: 10000
      }))
      this.app.configure(auth({
        jwtStrategy: 'auth0'
      }))
      await this.app.authenticate()
    } else {
      const restClient = rest(process.env.API_ENDPOINT)
      this.app.configure(restClient.axios(axios))
    }
    this.hasInit = true
  }

  service (name) {
    if (!this.hasInit) {
      throw new Error('Feathers not initiated')
    }
    return this.app.service(name)
  }
}

const feathersInstance = new Feathers()

export default feathersInstance
