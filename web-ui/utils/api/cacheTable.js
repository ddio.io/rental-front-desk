const DEFAULT_REFRESH_PERIOD = 10 * 60 * 1000

class CacheTable {
  constructor (refreshPeriod) {
    this.rows = []
    this.lastModified = 0
    this.refreshPeriod = refreshPeriod || DEFAULT_REFRESH_PERIOD
  }

  isOutdated () {
    return (new Date()) - this.lastModified > this.refreshPeriod
  }

  reset () {
    this.rows = []
  }

  push (...rows) {
    this.rows.push(...rows)
    this.lastModified = new Date()
  }

  get () {
    return this.rows
  }
}

export default CacheTable
