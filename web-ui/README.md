# Rental Front Desk Web UI

## Environment Variables

- Required:
  - `AUTH0_DOMAIN` & `AUTH0_CLIENT_ID`, for authentication
  - `API_ENDPOINT`, endpoint to API server
  - `FACEBOOK_PAGE_ID`, ID of facebook page for form redirect
- Optional:
  - `SENTRY_DSN`: Sentry DSN

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
