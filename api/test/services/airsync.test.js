const app = require('../../src/app')

describe('\'airsync\' service', () => {
  it('registered the service', () => {
    const service = app.service('airsync')
    expect(service).toBeTruthy()
  })
})
