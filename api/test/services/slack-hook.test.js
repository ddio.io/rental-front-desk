const app = require('../../src/app')

describe('\'slack-hook\' service', () => {
  it('registered the service', () => {
    const service = app.service('slack-hook')
    expect(service).toBeTruthy()
  })
})
