const app = require('../../src/app')

describe('\'slack-notify\' service', () => {
  it('registered the service', () => {
    const service = app.service('slack-notify')
    expect(service).toBeTruthy()
  })
})
