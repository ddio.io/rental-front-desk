# Frontdesk API

Provide all API using [Feathersjs](https://feathersjs.com/)

## System Requirement

1. Node 12 lts
2. PostgresSQL 12

## Setup Howto

### Environment Variable

Although Feathers.js provide app config using `node-config`, we still keep all credential related info in environment variable.

Use `.env` or env var to setup the following variable:

- Server and Database
  - `HOST`: Host name of server
  - `DATABASE_URL`: DB connection string
- Airtable Connection (See Feathers Config section for more detail)
  - `AT_TICKET_ID`: Airtable account ID for ticket table
  - `AT_TICKET_KEY`: Key of Airtable base for ticket table
  - `AT_SETTING_KEY`: Airtable account ID for setting related table
  - `AT_SETTING_ID`: Key of Airtable base for setting related table
- Chat bot
  - `MESSENGER_PAGE_ID`: Facebook Page to use
  - `MESSENGER_ACCESS_TOKEN`: Page access token
  - `MESSENGER_APP_ID` & `MESSENGER_APP_SECRET`: Facebook App ID
  - `MESSENGER_VERIFY_TOKEN`: Chatbot verify token
- UI Login
  - `CLIENT_ENDPOINTS`: comma separated list of login domain (for CORS)
- User Authentication
  - `AUTH0_DOMAIN`: Auth0 domain (without protocol) for JWT validation
- Option log
  - `SENTRY_DSN`: dsn of sentry

### Feathers Config

Except from regular API service config, we also put Airtable mapping in config file. There are 2 sections in airtable dictionary:

1. `auth` - define all possible Airtable access config.
2. `tables` - define mapping of each DB and Airtable table, and key of auth config.

## Dev Howto

```
npm i
npm run dev
```

## Production Howto

TBD

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```
