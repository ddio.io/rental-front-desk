module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'holiday',
      'isNotHoliday',
      Sequelize.BOOLEAN
    )
    await queryInterface.addColumn(
      'cannedmsg',
      'offStartSec',
      Sequelize.INTEGER
    )
    await queryInterface.addColumn(
      'cannedmsg',
      'offEndSec',
      Sequelize.INTEGER
    )
    await queryInterface.removeColumn(
      'holiday',
      'isHoliday'
    )
    await queryInterface.removeColumn(
      'cannedmsg',
      'closeTimeStart'
    )
    await queryInterface.removeColumn(
      'cannedmsg',
      'closeTimeEnd'
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'holiday',
      'isNotHoliday'
    )
    await queryInterface.removeColumn(
      'cannedmsg',
      'offStartSec'
    )
    await queryInterface.removeColumn(
      'cannedmsg',
      'offEndSec'
    )
    await queryInterface.addColumn(
      'holiday',
      'isHoliday',
      Sequelize.BOOLEAN
    )
    await queryInterface.addColumn(
      'cannedmsg',
      'closeTimeStart',
      Sequelize.DATE
    )
    await queryInterface.addColumn(
      'cannedmsg',
      'closeTimeEnd',
      Sequelize.DATE
    )
  }
}
