module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'botmsg',
      'order',
      Sequelize.INTEGER
    )
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'botmsg',
      'order'
    )
  }
}
