module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('airsync', 'service', {
      type: Sequelize.STRING,
      allowNull: false
    })
    const pgEnumDropQuery = queryInterface.QueryGenerator.pgEnumDrop(
      'airsync', 'service')

    await queryInterface.sequelize.query(pgEnumDropQuery)

    return queryInterface.changeColumn('airsync', 'service', {
      type: Sequelize.ENUM(
        'holiday',
        'ticket-state',
        'slack-mapping',
        'canned-msg',
        'booking',
        'ticket',
        'ALL_TABLES',
        'slack-hook'
      ),
      allowNull: false
    })
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('airsync', 'service', {
      type: Sequelize.STRING,
      allowNull: false
    })

    const pgEnumDropQuery = queryInterface.QueryGenerator.pgEnumDrop(
      'airsync', 'service')

    await queryInterface.sequelize.query(pgEnumDropQuery)
    return queryInterface.changeColumn('airsync', 'service', {
      type: Sequelize.ENUM(
        'holiday',
        'ticket-state',
        'slack-mapping',
        'canned-msg',
        'booking',
        'ticket',
        'ALL_TABLES'
      ),
      allowNull: false
    })
  }
}
