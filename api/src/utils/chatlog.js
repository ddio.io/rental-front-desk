const _ = require('lodash')

const FIELDS = [
  'createdAt',
  'receiver',
  'type',
  'data',
  'mid',
  'replyTo',
  'source',
  'isAutoReply',
  'options'
]

const ACCEPTABLE_SOURCE = {
  client: true,
  admin: true,
  system: true
}

class ChatLog {
  constructor (payload, source = 'client') {
    if (!ACCEPTABLE_SOURCE[source]) {
      throw new Error(`Invalid source: ${source}`)
    }
    this.createdAt = Date.now()
    this.source = source

    if (this.isFromClient) {
      const { type, data } = this.parseMessengerPayload(payload)
      this.type = type
      this.data = data
      if (payload.isMessage) {
        const msg = payload.message
        this.mid = msg.mid
        if (msg.replyTo) {
          this.replyTo = msg.replyTo.mid
        }
      }
    } else {
      this.type = payload.type
      this.data = payload.data
      if (payload.options) {
        this.options = payload.options
      }
      if (payload.isAutoReply) {
        this.isAutoReply = true
      }
    }
  }

  get isFromClient () {
    return this.source === 'client'
  }

  get isFromAdmin () {
    return this.source === 'admin'
  }

  get isFromSystem () {
    // system info, not sending to any bot channel
    return this.source === 'system'
  }

  valueOf () {
    return _.pick(this, FIELDS)
  }

  parseMessengerPayload (payload) {
    if (payload.isText) {
      return {
        type: 'text',
        data: payload.text
      }
    } else if (payload.isLikeSticker) {
      return {
        type: 'like',
        data: ''
      }
    } else if (payload.isImage) {
      return {
        type: 'image',
        data: payload.attachments
      }
    } else if (payload.isPostback) {
      return {
        type: 'postback',
        data: payload.postback
      }
    } else {
      throw new Error(`Unknown client payload: ${JSON.stringify(payload._rawEvent)}`)
    }
  }
}

module.exports = ChatLog
