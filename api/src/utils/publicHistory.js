const _ = require('lodash')

class PublicHistory {
  constructor (toExternalState, msg) {
    this.createdAt = Date.now()
    this.state = toExternalState
    this.msg = msg
  }

  valueOf () {
    return _.pick(this, [
      'state',
      'msg',
      'createdAt'
    ])
  }
}

module.exports = PublicHistory
