const { authenticate } = require('@feathersjs/authentication')
const { NotAuthenticated } = require('@feathersjs/errors')
const { logger } = require('../logger')

const publicServices = ['member-report', 'health']
const auth0Auth = authenticate('auth0')

function whiteListAuth (context) {
  const { method, params: { provider }, path } = context
  logger.debug(`[auth] [${method}] /${path}, provider: ${provider}`)
  if (provider === 'server') {
    return context
  }
  if (path === 'authentication' && method === 'create') {
    return context
  }
  if (publicServices.includes(path)) {
    return context
  }
  return auth0Auth(context)
}

function onlyInternal (context) {
  const { params: { provider } } = context
  if (provider === 'server') {
    return context
  }
  throw new NotAuthenticated()
}

module.exports = {
  whiteListAuth,
  onlyInternal
}
