const moment = require('moment-timezone')
// check every 5 min
const CHECK_PERIOD = 5 * 60 * 1000
const SETTING_ID = 'slackNotifier'
const MAX_BOOKING = 3

async function checkNewTicket (app, logger) {
  // report if cannot map report email and booking info
  const settingService = app.service('setting')
  const setting = await settingService.get(SETTING_ID, { provider: 'server' })
  const lastCheckTime = moment(setting.config.lastCheckTime || '2020-01-01')
  await settingService.update(
    SETTING_ID,
    {
      config: {
        ...setting.config,
        lastCheckTime: moment()
      }
    },
    { provider: 'server' }
  )

  const ticketService = app.service('ticket')
  const newTickets = await ticketService.find({
    query: {
      createdAt: {
        $gte: lastCheckTime
      }
    },
    paginate: false
  })

  const slackService = app.service('slack-hook')
  const bookingService = app.service('booking')
  newTickets.forEach(async (ticket) => {
    const trueBookingInfo = await bookingService.getLastBooking({
      email: ticket.reporterEmail,
      buildingId: ticket.buildingId,
      roomId: ticket.roomId
    })
    if (trueBookingInfo) {
      // got a valid ticket, no warning
      return
    }
    const atUpdatedAt = moment(ticket.atUpdatedAt).format('HH:mm')
    const msg = [
      `室友 *${ticket.reporter}* 在 ${atUpdatedAt} 送出的 *${ticket.taskType}* 通報，找不到對應的房間呦，可能是 *信箱或房號* 填錯了。`,
      `• 信箱： ${ticket.reporterEmail}`,
      `• 房間： ${ticket.buildingId} ${ticket.roomId}`,
      `• 通報資訊： ${app.get('uiEndpoint')}/item/${ticket.id}`
    ].join('\n')
    await slackService.create({
      channel: ticket.taskType,
      msg
    }, {
      provider: 'server'
    })
  })
}

async function handleBeginOfWorkDay (app, logger) {
  const settingService = app.service('setting')
  const setting = await settingService.get(SETTING_ID, { provider: 'server' })
  const today = moment().tz(app.get('officeTz')).format('YYYY-MM-DD')
  const lastWorkDate = setting.config.lastWorkDate
  await settingService.update(
    SETTING_ID,
    {
      config: {
        ...setting.config,
        lastWorkDate: today
      }
    },
    { provider: 'server' }
  )
  if (!lastWorkDate || today === lastWorkDate) {
    // only do start-of-work check once && not first init
    logger.debug(`[BEG] Not begin of work day, skip. ${today} === ${lastWorkDate}`)
    return false
  }

  const chatService = app.service('member-chat')
  const unreadChats = await chatService.find({
    query: {
      isRead: false
    }
  })
  const bookings = unreadChats.map(chat => chat.booking)
  if (!bookings.length) {
    // beginning of work day, but no unread msg
    logger.debug('[BEG] No unread msg, good!')
    return true
  }
  let msg = '早安！截至今天開工前，共有'
  if (bookings.length < MAX_BOOKING) {
    msg += bookings.map(booking => booking.username).join('、') + '的訊息未讀'
  } else if (bookings.length === MAX_BOOKING) {
    msg += bookings.map(booking => booking.username).join('、') + ` ${MAX_BOOKING} 人的訊息未讀`
  } else {
    const headOfBookings = bookings.slice(0, MAX_BOOKING).map(booking => booking.username).join('、')
    msg += `${headOfBookings}...等 ${MAX_BOOKING} 人的訊息未讀`
  }
  msg += '，有空時記得回覆呦\n ( ･_･)♡'

  const slackService = app.service('slack-hook')
  await slackService.create({ msg }, { provider: 'server' })
  logger.debug(`[BEG] send msg: ${msg}`)
  return true
}

// periodic checker
//   -> report new msg on start of day
//   -> report timeout msg during working hour
async function checkRespTimeout (app, logger) {
  const isOnWorkHour = await app.service('holiday').isOnWorkHour()
  if (!isOnWorkHour) {
    // don't bother when it's holiday
    return
  }

  const isBeginningOfWorkDay = await handleBeginOfWorkDay(app, logger)
  if (isBeginningOfWorkDay) {
    // only do timeout check during work hour

  }
}

async function keepNotifySlack (app, logger) {
  await checkNewTicket(app, logger)
  await checkRespTimeout(app, logger)
  setInterval(() => {
    checkNewTicket(app, logger)
    checkRespTimeout(app, logger)
  }, CHECK_PERIOD)
}

module.exports = {
  keepNotifySlack
}
