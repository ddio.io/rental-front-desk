const msgCache = {}

const syncCallbackList = {}

async function syncMsgCache (app, logger) {
  const botMsgService = app.service('bot-msg')

  const botMsgList = await botMsgService.find({
    query: {
      $sort: {
        key: 1
      }
    },
    provider: 'server',
    paginate: false
  })

  for (const category in syncCallbackList) {
    logger.debug(`Sync ${category}`)
    const sync = syncCallbackList[category]
    sync({
      app,
      logger,
      categoryCache: msgCache[category],
      botMsgList
    })
  }
}

async function keepMsgCacheSync (app, logger) {
  const syncPeriod = app.get('syncPeriodMinute') * 60 * 1000
  await syncMsgCache(app, logger)
  setInterval(async () => {
    await syncMsgCache(app, logger)
  }, syncPeriod)
}

// register template of specific category
// params:
//    - syncCallback: ({ app, logger, categoryCache, botMsgList })
// return: pointer to that category
function registerMsgCache (category, template, syncCallback) {
  msgCache[category] = Object.assign({}, template)
  syncCallbackList[category] = syncCallback
  return msgCache[category]
}

module.exports = {
  keepMsgCacheSync,
  registerMsgCache
}
