const { chain } = require('bottender')

const onboardHandler = require('./onboardHandler')
const checkMember = require('./checkMember')
const msgHandler = require('./msgHandler')
const reportHandler = require('./reportHandler')
const nonMemberHandler = require('./nonMemberHandler')

// async function defaultHandler (context, { next }) {
//   const ev = context.event
//   console.log('[*] ', ev._rawEvent)
// }

module.exports = function App () {
  return chain([
    onboardHandler,
    nonMemberHandler,
    checkMember,
    reportHandler,
    msgHandler
  ])
}
