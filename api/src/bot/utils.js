const ChatLog = require('../utils/chatlog')
const global = require('../utils/global')

async function appendReply ({ context, replyText, isAutoReply = true, options = {} }) {
  const app = global.getItem('app')
  const chatService = app.service('chat')
  const userId = context.session.user.id

  const autoReply = new ChatLog({
    type: 'text',
    data: replyText,
    isAutoReply,
    options
  }, 'admin')

  await chatService.append({
    channelId: userId,
    chatLog: autoReply
  })
}

module.exports = {
  appendReply
}
