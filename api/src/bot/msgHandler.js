const moment = require('moment-timezone')
const global = require('../utils/global')
const ChatLog = require('../utils/chatlog')

const AUTO_REPLY_CD_HOURS = 24

async function needTimeOffReply (context) {
  const app = global.getItem('app')
  const holidayService = app.service('holiday')
  const botMsgService = app.service('bot-msg')
  const today = moment().tz('Asia/Taipei')
  const isOnHour = await holidayService.isOnWorkHour(today)

  if (isOnHour) {
    return false
  }

  // we are off, check if we have auto reply recently, or send auto reply
  const offSetting = await botMsgService.Model.findOne({
    where: {
      key: '下班自動回覆',
      category: '自動回覆'
    }
  })

  const autoReplyIfBefore = today.clone().subtract(AUTO_REPLY_CD_HOURS, 'hours')
  const lastOffTime = context.state.lastOffTime
  if (lastOffTime && moment(lastOffTime) > autoReplyIfBefore) {
    // it's holiday or off hour, but we've auto reply recently
    return false
  }
  context.setState({
    lastOffTime: today
  })
  return offSetting.content
}

async function backupAndNotify (context) {
  const chat = new ChatLog(context.event)
  const userId = context.session.user.id
  const app = global.getItem('app')
  const chatService = app.service('chat')

  await chatService.append({
    channelId: userId,
    chatLog: chat
  })

  const offTimeReply = await needTimeOffReply(context)
  if (offTimeReply) {
    const autoReply = new ChatLog({
      type: 'text',
      data: offTimeReply,
      isAutoReply: true
    }, 'admin')
    await chatService.append({
      channelId: userId,
      chatLog: autoReply
    })
  }
}

async function msgHandler (context, { next }) {
  if (context.event.isDelivery) {
    return next
  } else if (context.state.bookingInfo) {
    return backupAndNotify(context)
  } else {
    return next
  }
}

module.exports = msgHandler
