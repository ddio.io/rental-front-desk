const moment = require('moment')
const global = require('../utils/global')
const ChatLog = require('../utils/chatlog')
const { logger } = require('../logger')
const { registerMsgCache } = require('./botMsgCache')
const { appendReply } = require('./utils')

const contentTimer = {}

const SUBMIT_CONTENT_KEYWORDS = new RegExp(/提交通報|提交通报|Submit!/i)

const BINARY_REPLY = {
  en: [
    { contentType: 'text', title: 'Yes', payload: 'REPLY_YES' },
    { contentType: 'text', title: 'No', payload: 'REPLY_NO' }
  ],
  zh: [
    { contentType: 'text', title: '是', payload: 'REPLY_YES' },
    { contentType: 'text', title: '否', payload: 'REPLY_NO' }
  ]
}

const msgCacheTpl = {
  trigger: {},
  question: {
    selectCategory: { zh: '', en: '' },
    addContent: { zh: '', en: '' },
    addContentEmergency: { zh: '', en: '' },
    done: { zh: '', en: '' }
  },
  category: {
    REPORT_FIX: [
      // { key: '', name: { en: '', zh: '' }, reply: { en: '', zh: '' } }
    ],
    REPORT_MISC: [
      // { key: '', name: { en: '', zh: '' }, reply: { en: '', zh: '' } }
    ]
  }
}

function syncMsgCache ({ categoryCache, botMsgList }) {
  // update trigger
  const triggers = botMsgList.filter((msg) => {
    return msg.category === '選單' && msg.key !== '非室友選單'
  })

  categoryCache.trigger = {}
  triggers.forEach((row) => {
    categoryCache.trigger[row.payload] = true
  })

  // update question
  const questions = botMsgList.filter(msg => msg.category === '新增通報')

  questions.forEach((row) => {
    if (row.key in msgDef.translate) {
      const question = msgDef.translate[row.key]
      const target = categoryCache.question[question]
      if (row.language === '華語') {
        target.zh = row.content
      } else {
        target.en = row.content
      }
    }
  })

  // update category and auto reply per category
  const autoReplyList = botMsgList
    .filter(msg => msg.category === '自動回覆' && msg.order)
    .sort((l, r) => {
      return l.order - r.order
    })
  autoReplyList.forEach((reply) => {
    // payload === EN_REPORT_MISC
    const categoryKey = reply.payload.split('_').slice(1).join('_')
    const language = reply.payload.split('_')[0].toLowerCase()
    if (!categoryCache.category[categoryKey]) {
      console.error(`Invalid auto reply category: ${categoryKey}`)
      return
    }
    const category = categoryCache.category[categoryKey]
    let oneItem = category.find(item => item.key === reply.key)
    if (!oneItem) {
      oneItem = {
        key: reply.key,
        name: {
          en: en(reply.key),
          zh: zh(reply.key)
        },
        reply: {
          en: '',
          zh: ''
        }
      }
      category.push(oneItem)
    }
    if (reply.content) {
      oneItem.reply[language] = reply.content
    }
  })
}

const categoryCache = registerMsgCache('report', msgCacheTpl, syncMsgCache)

const msgDef = {
  translate: {
    選擇通報類型: 'selectCategory',
    通報完成預設回覆: 'done',
    填寫通報內容: 'addContent',
    '填寫通報內容-緊急事件': 'addContentEmergency'
  },
  category: {
    REPORT_EMERGENCY: [
      '緊急事件 Emergencies'
    ]
  }
}

function zh (str) {
  return str.split(' ')[0]
}

function en (str) {
  return str.split(' ').slice(1).join(' ')
}

async function appendReportReply (context, replyText, includeCategoryList = false) {
  let options = {}

  if (includeCategoryList) {
    const categoryList = context.state.report.categoryList
    const payloadList = context.state.report.categoryListPayload
    options = {
      quickReplies: categoryList.map((text, i) => ({
        contentType: 'text',
        title: text,
        payload: payloadList[i].key
      }))
    }
  }

  await appendReply({
    context,
    replyText,
    options
  })
}

async function handleSelectCategory (context, isInit) {
  const event = context.event
  const reportState = context.state.report
  let text = (event.text || '').trim()
  if (event.isQuickReply) {
    text = event.quickReply.payload
  } else {
    // users type themselves
    text = reportState.categoryListPayload.find(item => item.key.includes(text))
    if (text) {
      text = text.key
    }
  }
  if (isInit || !text) {
    const msg = categoryCache.question.selectCategory[reportState.language]
    await appendReportReply(context, msg, true)
  } else {
    const categoryPayload = reportState.categoryListPayload.find(item => item.key === text)
    await context.setState({
      report: {
        ...reportState,
        categoryPayload,
        category: text
      }
    })
    if (categoryPayload.reply[reportState.language]) {
      handleAutoReply(context, true)
    } else {
      handleAddContent(context, true)
    }
  }
}

async function finishReportWheTimeout (context) {
  // TODO: add some hint to user
  await handleDoneReport(context)
}

function startAddContentTimeout (context) {
  const app = global.getItem('app')
  const contentTimeout = app.get('addContentTimeoutMinute') * 60 * 1000
  const userId = context.session.user.id
  contentTimer[userId] = setTimeout(() => {
    finishReportWheTimeout(context)
  }, contentTimeout)
}

function clearAddContentTimeout (context) {
  const userId = context.session.user.id
  if (contentTimer[userId]) {
    clearTimeout(contentTimer[userId])
    delete contentTimer[userId]
  }
}

async function handleAutoReply (context, isInit) {
  const reportState = context.state.report
  if (isInit) {
    await context.setState({
      report: {
        ...reportState,
        step: 'autoReply'
      }
    })
    const language = reportState.language
    const msg = reportState.categoryPayload.reply[language]
    await appendReply({
      context,
      replyText: msg,
      options: {
        quickReplies: BINARY_REPLY[language]
      }
    })
    return
  }
  const event = context.event
  let text = (event.text || '').trim().toLowerCase()
  if (event.isQuickReply) {
    text = event.quickReply.payload
  } else {
    text = text === 'Yes' || text === '是' ? 'REPLY_YES' : 'REPLY_NO'
  }
  if (text === 'REPLY_YES') {
    // TODO: submit to airtable
    await context.setState({
      report: {
        ...reportState,
        textContent: ['自動回覆已解決'],
        imageContent: []
      }
    })
    await handleDoneReport(context, '問題已排除-觀察中')
  } else {
    await handleAddContent(context, true)
  }
}

async function handleAddContent (context, isInit) {
  const reportState = context.state.report
  if (isInit) {
    await context.setState({
      report: {
        ...reportState,
        step: 'addContent'
      }
    })
    let msg = categoryCache.question.addContent[reportState.language]
    if (reportState.category === msgDef.category.REPORT_EMERGENCY[0]) {
      msg = categoryCache.question.addContentEmergency[reportState.language]
    }
    await appendReportReply(context, msg)
    startAddContentTimeout(context)
  } else {
    const event = context.event
    const textContent = reportState.textContent || []
    const imageContent = reportState.imageContent || []

    if (event.isText) {
      textContent.push(event.text)
    } else if (event.isImage) {
      imageContent.push(...event.attachments.map((image) => {
        return {
          url: image.payload.url
        }
      }))
    }

    await context.setState({
      report: {
        ...reportState,
        textContent,
        imageContent
      }
    })

    if (event.isText && event.text.match(SUBMIT_CONTENT_KEYWORDS)) {
      await handleDoneReport(context)
    } else {
      startAddContentTimeout(context)
    }
  }
}

async function handleDoneReport (context, state = '新通報') {
  // send report to airtable
  const app = global.getItem('app')
  const airsyncService = app.service('airsync')
  const ticketService = app.service('ticket')
  const booking = context.state.bookingInfo
  const report = context.state.report
  const ticketData = {
    // TODO: set default state
    state,
    buildingId: booking.buildingId,
    roomId: booking.roomId,
    taskType: report.category,
    reporter: booking.username,
    reporterEmail: booking.email,
    reportedAt: moment(),
    content: report.textContent.join('\n'),
    contentAttachment: report.imageContent
  }
  try {
    const atRow = await airsyncService.createServiceRow('ticket', ticketData)
    await ticketService.create({
      ...ticketData,
      ...atRow
    })
  } catch (error) {
    logger.error(error)
  }
  // reply to member
  const doneMsg = categoryCache.question.done[report.language]
  await appendReportReply(context, doneMsg)
  // clear state
  await context.setState({
    report: false
  })
}

async function handleReport (context, next) {
  const reportState = context.state.report
  if (reportState.step === 'selectCategory') {
    await handleSelectCategory(context)
  } else if (reportState.step === 'autoReply') {
    await handleAutoReply(context)
  } else if (reportState.step === 'addContent') {
    await handleAddContent(context)
  } else {
    return next
  }
}

async function handleInitReport (context) {
  const postback = context.event.postback
  if (!postback) {
    return
  }
  // payload === EN_REPORT_MISC
  const payloadToken = postback.payload.split('_')
  const language = payloadToken[0].toLowerCase()
  const type = payloadToken.slice(1).join('_')
  const reportState = {
    type,
    language
  }

  if (type === 'REPORT_EMERGENCY') {
    await context.setState({
      report: {
        ...reportState,
        category: msgDef.category.REPORT_EMERGENCY[0]
      }
    })
    await handleAddContent(context, true)
  } else if (categoryCache.category[type]) {
    // show category
    const categoryListPayload = categoryCache.category[type]
    const categoryList = categoryListPayload.map(info => info.name[language])
    await context.setState({
      report: {
        ...reportState,
        step: 'selectCategory',
        categoryList,
        categoryListPayload
      }
    })
    await handleSelectCategory(context, true)
  } else {
    throw new Error(`Undefined postback action for ${type}-${language}`)
  }
}

function isTriggerPayload (context) {
  if (!context.event.isPostback) {
    return false
  }
  const postback = context.event.postback
  return postback.payload in categoryCache.trigger
}

async function reportHandler (context, { next }) {
  if (!context.state.bookingInfo || context.event.isDelivery) {
    return next
  }

  let nextStep = null
  if (isTriggerPayload(context)) {
    nextStep = handleInitReport
  } else if (context.state.report) {
    nextStep = handleReport
  }
  if (nextStep) {
    const userId = context.session.user.id
    const app = global.getItem('app')
    const chatService = app.service('chat')
    const chat = new ChatLog(context.event)
    await chatService.append({
      channelId: userId,
      chatLog: chat
    })
    clearAddContentTimeout(context)
    return nextStep(context, next)
  }

  return next
}

module.exports = reportHandler
