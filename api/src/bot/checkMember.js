const global = require('../utils/global')

async function checkMember (context, { next }) {
  if (context.state.noBooking || context.event.isDelivery || context.state.bookingInfo) {
    // TODO: handle member removal
    return next
  }
  // we handle only member who has booking record
  // only check this if we don't know user status to reduce resp time
  const userId = context.session.user.id
  const app = global.getItem('app')
  const bookingService = app.service('booking')

  const bookings = await bookingService.find({
    query: {
      channelType: 'facebook',
      channelId: userId,
      $sort: {
        updatedAt: -1
      },
      $limit: 1
    }
  })

  if (bookings.total > 0) {
    context.setState({
      bookingInfo: bookings.data[0],
      noBooking: false
    })
  } else {
    context.setState({
      noBooking: true
    })
  }

  return next
}

module.exports = checkMember
