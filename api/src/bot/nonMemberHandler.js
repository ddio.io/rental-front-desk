
const { registerMsgCache } = require('./botMsgCache')
const { appendReply } = require('./utils')

const msgTpl = {
  // list of postback payload
}

function syncMsg ({ categoryCache, botMsgList }) {
  const msgs = botMsgList.filter((msg) => {
    return msg.category === '自動回覆' && msg.key === '非室友選單回覆'
  })

  msgs.forEach((msg) => {
    categoryCache[msg.payload] = msg.content
  })
}

const categoryCache = registerMsgCache('nonMember', msgTpl, syncMsg)

async function nonMemberHandler (context, { next }) {
  const event = context.event
  if (!event.isPostback) {
    return next
  }
  const payload = event.postback.payload
  if (payload in categoryCache) {
    await appendReply({
      context,
      replyText: categoryCache[payload]
    })
    return
  }

  return next
}

module.exports = nonMemberHandler
