// Initializes the `slackmapping` service on path `/slack-mapping`
const { SlackMapping } = require('./slackMapping.class')
const createModel = require('../../models/slackMapping.model')
const hooks = require('./slackMapping.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/slack-mapping', new SlackMapping(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('slack-mapping')

  service.hooks(hooks)
}
