const { NotAuthenticated, NotFound } = require('@feathersjs/errors')

function providerChecker (context) {
  if (context.params.provider === 'server') {
    return context
  }
  const restriction = context.service.serviceRestriction
  if (!(context.id in restriction)) {
    throw new NotFound()
  }
  if (restriction[context.id]) {
    throw new NotAuthenticated()
  }
  return context
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [providerChecker],
    create: [],
    update: [providerChecker],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
