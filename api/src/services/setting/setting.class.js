const { Service } = require('feathers-sequelize')
const { NotImplemented, NotAuthenticated, NotFound } = require('@feathersjs/errors')

exports.Setting = class Setting extends Service {
  constructor (...args) {
    super(...args)
    // list of service setting, and whether require server provider
    this.serviceRestriction = {
      slackNotifier: true
    }
  }

  find () {
    throw new NotImplemented()
  }

  patch () {
    throw new NotImplemented()
  }

  create (data, params) {
    if (params.provider !== 'server') {
      throw new NotAuthenticated()
    }
    return super.create(data, params)
  }

  async get (id, params) {
    try {
      const row = await super.get(id, params)
      return row
    } catch (err) {
      if (err instanceof NotFound && id in this.serviceRestriction) {
        return this.create({
          service: id,
          config: {}
        }, {
          ...params,
          provider: 'server'
        })
      } else {
        throw err
      }
    }
  }

  async update (id, data, params) {
    // check if the id is valid
    await this.get(id, params)
    return super.update(id, data, params)
  }
}
