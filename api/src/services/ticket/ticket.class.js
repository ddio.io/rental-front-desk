const { Service } = require('feathers-sequelize')

exports.Ticket = class Ticket extends Service {
  setup (app) {
    this.app = app
  }

  async getBooking (ticket) {
    const relatedBooking = await this.app.service('booking').find({
      query: {
        buildingId: ticket.buildingId,
        roomId: ticket.roomId,
        email: ticket.reporterEmail,
        $limit: 1,
        $sort: { updatedAt: -1 }
      }
    })
    if (relatedBooking.total > 0) {
      return relatedBooking.data[0]
    }
    return null
  }

  async get (id, params) {
    const row = await super.get(id, params)
    if (!row.reporterEmail || !row.roomId || !row.buildingId) {
      return row
    }
    row.booking = await this.getBooking(row)
    return row
  }

  async patch (id, data, params) {
    const airsync = this.app.service('airsync')
    await airsync.uploadServiceRow('ticket', {
      ...data,
      id
    })
    return super.patch(id, data, params)
  }
}
