exports.Health = class Health {
  constructor (options) {
    this.options = options || {}
  }

  setup (app) {
    this.app = app
  }

  async find (params) {
    const syncJobs = await this.app.service('airsync').find({
      query: {
        service: 'ALL_TABLES',
        $limit: 1,
        $sort: {
          createdAt: -1
        }
      },
      paginate: false
    })

    const ret = {
      succeeded: true,
      hasSync: !!syncJobs
    }

    if (syncJobs.length) {
      ret.lastSync = syncJobs[0]
    }

    return ret
  }
}
