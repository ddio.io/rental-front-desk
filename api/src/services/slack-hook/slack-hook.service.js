// Initializes the `slack-hook` service on path `/slack-hook`
const { SlackHook } = require('./slack-hook.class')
const createModel = require('../../models/slack-hook.model')
const hooks = require('./slack-hook.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/slack-hook', new SlackHook(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('slack-hook')

  service.hooks(hooks)
}
