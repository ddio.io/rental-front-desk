const { Service } = require('feathers-sequelize')
const { NotImplemented, NotAuthenticated, BadRequest } = require('@feathersjs/errors')
const axios = require('axios')

exports.SlackHook = class SlackHook extends Service {
  find () {
    throw new NotImplemented()
  }

  get () {
    throw new NotImplemented()
  }

  update () {
    throw new NotImplemented()
  }

  patch () {
    throw new NotImplemented()
  }

  delete () {
    throw new NotImplemented()
  }

  async create (data, params) {
    if (params.provider !== 'server') {
      throw new NotAuthenticated()
    }
    if (Array.isArray(data)) {
      const resp = []
      for (const i in data) {
        resp.push(await this.create(data[i], params))
      }
      return resp
    }
    if (!data.msg) {
      throw new BadRequest('Missing required parameter')
    }
    data.channel = data.channel || '*'
    let hook = await this.Model.findOne({
      where: {
        channel: data.channel
      }
    })
    if (!hook) {
      hook = await this.Model.findOne({
        where: {
          channel: '*'
        }
      })
    }
    if (!hook) {
      throw new BadRequest(`Channel ${data.channel} not found`)
    }
    let payload = {}
    if (typeof data.msg === 'string') {
      payload.text = data.msg
      payload.type = 'mrkdwn'
    } else {
      payload = data.msg
    }
    payload = {
      blocks: [{
        type: 'section',
        text: payload
      }]
    }
    const resp = await axios.post(hook.webhook, payload)
    return {
      result: resp.data
    }
  }
}
