// Initializes the `bot-msg` service on path `/bot-msg`
const { BotMsg } = require('./bot-msg.class')
const createModel = require('../../models/bot-msg.model')
const hooks = require('./bot-msg.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/bot-msg', new BotMsg(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('bot-msg')

  service.hooks(hooks)
}
