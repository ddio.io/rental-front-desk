const { Service } = require('feathers-sequelize')
const { NotImplemented } = require('@feathersjs/errors')
const moment = require('moment-timezone')
const { logger } = require('../../logger')

const CACHE_SIZE = 512
const CACHE_BUFFER = 2
const resultCache = {}

exports.Holiday = class Holiday extends Service {
  setup (app) {
    this.app = app
  }

  // internal only, no API supported
  find () {
    throw new NotImplemented()
  }

  create () {
    throw new NotImplemented()
  }

  patch () {
    throw new NotImplemented()
  }

  remove () {
    throw new NotImplemented()
  }

  updateCache (resp) {
    resultCache[resp.targetDate] = resp
    const allDates = Object.keys(resultCache)
    if (allDates.length >= CACHE_SIZE * CACHE_BUFFER) {
      const oldDates = allDates.slice(0, allDates.length - CACHE_SIZE)
      oldDates.forEach((date) => {
        delete resultCache[date]
      })
    }
    return resp
  }

  async get (id) {
    const targetDate = moment(id || undefined).tz(this.app.get('officeTz'))
    const targetDateStr = targetDate.format('YYYY-MM-DD')
    if (resultCache[targetDateStr]) {
      return resultCache[targetDateStr]
    }
    const manualHolidayRow = await this.Model.findOne({
      where: {
        targetDate: targetDateStr
      }
    })

    if (manualHolidayRow) {
      return this.updateCache({
        targetDate: targetDateStr,
        isHoliday: !manualHolidayRow.isNotHoliday
      })
    }
    const dayOfWeek = targetDate.format('d')
    return this.updateCache({
      targetDate: targetDateStr,
      isHoliday: dayOfWeek % 6 === 0
    })
  }

  async isOnWorkHour (time) {
    time = moment(time).tz(this.app.get('officeTz'))
    const holidayInfo = await this.get(time)
    logger.debug(`[Holiday] it's ${time.format()} in tz ${this.app.get('officeTz')}`)
    logger.debug(`[Holiday] and db say isHoliday = ${holidayInfo.isHoliday}`)
    if (holidayInfo.isHoliday) {
      return false
    }

    const botMsgService = this.app.service('bot-msg')
    const offSetting = await botMsgService.Model.findOne({
      where: {
        key: '下班自動回覆',
        category: '自動回覆'
      }
    })
    logger.debug(`[Holiday] is working day, so office hour = ${offSetting.offEndSec} - ${offSetting.offStartSec}`)
    if (offSetting && 'offStartSec' in offSetting && 'offEndSec' in offSetting) {
      const todayOffFrom = time.clone().startOf('day').seconds(offSetting.offStartSec)
      const todayOffTo = time.clone().startOf('day').seconds(offSetting.offEndSec)
      logger.debug(`[Holiday] working hour: ${todayOffTo.format()} - ${todayOffFrom.format()}`)
      if (time < todayOffFrom && time > todayOffTo) {
        // not holiday && on hour, we are done :)
        logger.debug('[Holiday] we are in work hour')
        return true
      }
    }
    // not holiday && off hour
    logger.debug('[Holiday] we are NOT in work hour')
    return false
  }
}
