// Initializes the `holiday` service on path `/holiday`
const { Holiday } = require('./holiday.class')
const createModel = require('../../models/holiday.model')
const hooks = require('./holiday.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/holiday', new Holiday(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('holiday')

  service.hooks(hooks)
}
