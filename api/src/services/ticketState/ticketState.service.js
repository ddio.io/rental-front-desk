// Initializes the `ticketstate` service on path `/ticket-state`
const { TicketState } = require('./ticketState.class')
const createModel = require('../../models/ticketState.model')
const hooks = require('./ticketState.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/ticket-state', new TicketState(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('ticket-state')

  service.hooks(hooks)
}
