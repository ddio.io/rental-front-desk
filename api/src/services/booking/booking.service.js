// Initializes the `booking` service on path `/booking`
const { Booking } = require('./booking.class')
const createModel = require('../../models/booking.model')
const hooks = require('./booking.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/booking', new Booking(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('booking')

  service.hooks(hooks)
}
