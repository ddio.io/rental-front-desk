const { Service } = require('feathers-sequelize')
const { NotAuthenticated, NotImplemented } = require('@feathersjs/errors')

// if got multiple room with the same email, favor latest airtable updated one
// if got multiple email with the same room, favor latest airtable updated one

exports.Booking = class Booking extends Service {
  setup (app) {
    this.app = app
  }

  update () {
    throw new NotImplemented()
  }

  remove () {
    throw new NotImplemented()
  }

  async patch (id, data, params) {
    // only allow triggered from non http interface
    if (!params.provider || params.provider !== 'server') {
      throw new NotAuthenticated()
    }
    const airsync = this.app.service('airsync')
    await airsync.uploadServiceRow('booking', {
      ...data,
      id
    })
    return super.patch(id, data, params)
  }

  async create (data, params) {
    // only allow triggered from non http interface
    if (!params.provider || params.provider !== 'server') {
      throw new NotAuthenticated()
    }

    const airsync = this.app.service('airsync')
    const atRow = await airsync.createServiceRow('booking', data)
    return super.create({
      ...data,
      id: atRow.id
    }, params)
  }

  getLastBooking (whereCriteria) {
    return this.Model.findOne({
      where: whereCriteria,
      order: [
        ['atUpdatedAt', 'DESC']
      ]
    })
  }

  async getMemberIds (channelType) {
    const memberRows = await this.Model.findAll({
      attributes: ['channelId'],
      group: 'channelId',
      where: {
        channelType
      }
    })
    return memberRows.map(row => row.channelId)
  }

  async getFloors () {
    const floorRows = await this.Model.findAll({
      attributes: ['floor'],
      group: 'floor',
      order: [
        ['floor', 'ASC']
      ]
    })
    return floorRows.map(row => row.floor)
  }

  async getRoomsInFloor (floor) {
    const floorRows = await this.Model.findAll({
      attributes: ['roomId'],
      group: 'roomId',
      where: {
        floor: floor
      },
      order: [
        ['roomId', 'ASC']
      ]
    })
    return floorRows.map(row => row.roomId)
  }
}
