// Initializes the `airsync` service on path `/airsync`
const { Airsync } = require('./airsync.class')
const createModel = require('../../models/airsync.model')
const hooks = require('./airsync.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/airsync', new Airsync(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('airsync')

  service.hooks(hooks)
}
