const { Service } = require('feathers-sequelize')
const { NotImplemented, BadRequest, NotAuthenticated } = require('@feathersjs/errors')
const moment = require('moment')
const { Op } = require('sequelize')
const Airtable = require('airtable')
const _ = require('lodash')
const { logger } = require('../../logger')

const ALL_TABLES = 'ALL_TABLES'

const TOO_LONG_AGO_MIN = 10

const dataDecoder = {
  date (val) {
    return moment(val)
  },
  dateOnly (val) {
    return moment(val)
  },
  json (val) {
    if (typeof val !== 'string') {
      return val
    }
    try {
      return JSON.parse(val)
    } catch (err) {
      logger.error(`Error when parsing json string: ${val}`)
    }
    return null
  }
}

const dataEncoder = {
  dateOnly (val) {
    return moment(val).format('YYYY-MM-DD')
  },
  date (val) {
    return moment(val).toISOString()
  },
  json (val) {
    return JSON.stringify(val)
  }
}

exports.Airsync = class Airsync extends Service {
  constructor (options, app) {
    super(options, app)
    const atConfig = app.get('airtable')
    this.auth = atConfig.auth
    this.tables = atConfig.tables
    this.app = app
  }

  async create (data, params) {
    // only allow triggered from non http interface
    // if (!params.provider || params.provider !== 'server') {
    //   throw new NotAuthenticated()
    // }
    if (Array.isArray(data)) {
      // create service sequentially to avoid Airtable rate limit
      const resp = []
      for (const i in data) {
        resp.push(await this.create(data[i], params))
      }
      return resp
    }

    // only accepted table in config
    if (
      !data.service ||
      (!this.tables[data.service] && data.service !== ALL_TABLES)
    ) {
      throw new BadRequest('Undefined service')
    }
    // only start new job when now such service is syncing
    if (await this.getUnfinishedJob(data.service)) {
      throw new BadRequest('Service syncing in progress')
    }
    // start syncing
    await super.create(data, params)
    const resp = await this.downloadService(data.service)
    return resp
  }

  patch () {
    throw new NotImplemented()
  }

  update () {
    throw new NotImplemented()
  }

  async remove (id, params) {
    if (params.provider !== 'server') {
      return new NotAuthenticated()
    }
    return super.remove(id, params)
  }

  cleanup (days = 7) {
    return this.Model.destroy({
      where: {
        createdAt: {
          [Op.lte]: moment().subtract(days, 'days')
        }
      }
    })
  }

  async getUnfinishedJob (service) {
    const existingJob = await this.Model.findOne({
      where: {
        service: service,
        status: 'doing',
        updatedAt: {
          [Op.gte]: moment().subtract(TOO_LONG_AGO_MIN, 'minutes')
        }
      }
    })
    return existingJob
  }

  getAtTable (service) {
    const config = this.tables[service]
    const auth = this.auth[config.auth]
    const atBase = new Airtable({ apiKey: auth.key }).base(auth.base)
    return atBase(config.name)
  }

  async downloadService (service) {
    const config = this.tables[service]
    const theJob = await this.getUnfinishedJob(service)

    if (service === ALL_TABLES) {
      const tableList = Object.keys(this.tables)
      try {
        for (const i in tableList) {
          await this.downloadService(tableList[i])
        }
      } catch (error) {
        logger.error(error)
        if (theJob) {
          theJob.status = 'failed'
          await theJob.save()
          return theJob
        }
      }
      theJob.status = 'succeeded'
      await theJob.save()
      return theJob
    }

    const airtable = this.getAtTable(service)
    const modelName = service.replace(/-/g, '')

    const sequelize = this.app.get('sequelizeClient')
    const dbTable = sequelize.models[modelName]
    const transaction = await sequelize.transaction()

    return new Promise((resolve) => {
      const atIdList = []
      airtable
        .select({ view: config.view || 'Grid view' })
        .eachPage(
          async (records, fetchNextPage) => {
            await Promise.all(records.map(async (row) => {
              atIdList.push(row.id)
              try {
                await this.updateDbIfNeeded({
                  airtableRow: row,
                  dbTable,
                  dbConfig: config,
                  transaction
                })
              } catch (err) {
                logger.error(`Error while sync row: ${err}`)
              }
            }))
            fetchNextPage()
          },
          async (err) => {
            if (err) {
              logger.error(`Error during retrieve airtable ${err}`)
            }
            await this.removeOutdatedDbRow({
              dbTable,
              existingAtIds: atIdList,
              transaction
            })
            await transaction.commit()
            if (theJob) {
              theJob.status = err ? 'failed' : 'succeeded'
              await theJob.save()
            }
            resolve(theJob)
          }
        )
    })
  }

  async removeOutdatedDbRow ({ dbTable, existingAtIds, transaction }) {
    await dbTable.destroy({
      where: {
        id: {
          [Op.notIn]: existingAtIds
        }
      },
      transaction
    })
  }

  async updateDbIfNeeded ({ airtableRow, dbTable, dbConfig, transaction }) {
    const dbToAtMap = dbConfig.map
    const decoder = dbConfig.decoder || {}
    const tsField = dbToAtMap.atUpdatedAt

    const fields = Object.keys(airtableRow.fields)

    if (fields.length === 0) {
      // skip empty row
      return
    }
    if (dbConfig.required) {
      const requiredAtColumns = dbConfig.required.map(field => dbToAtMap[field])
      const requiredInAtRow = _.intersection(fields, requiredAtColumns)
      if (requiredInAtRow.length !== requiredAtColumns.length) {
        // ignore uncompleted row
        return
      }
    }

    const [dbRow, created] = await dbTable.findOrCreate({
      where: { id: airtableRow.id },
      transaction
    })
    let isDirty = false
    if (tsField && airtableRow.get(tsField)) {
      const atTs = moment(airtableRow.get(tsField))
      const dbTs = moment(dbRow.updatedAt)
      isDirty = (atTs > dbTs || created)
      dbRow.updatedAt = atTs
    } else {
      isDirty = true
    }
    if (isDirty) {
      Object.keys(dbToAtMap).forEach(dbKey => {
        const atValue = airtableRow.get(dbToAtMap[dbKey])
        if (decoder[dbKey]) {
          dbRow[dbKey] = this.decode(atValue, decoder[dbKey])
        } else {
          dbRow[dbKey] = atValue
        }
      })
      const defaultValue = dbConfig.defaultValue
      if (defaultValue) {
        Object.keys(defaultValue).forEach((key) => {
          if (!(dbToAtMap[key] in airtableRow.fields) || airtableRow.fields[dbToAtMap[key]] === null) {
            dbRow[key] = defaultValue[key]
          }
        })
      }
      await dbRow.save({ transaction })
    }
  }

  decode (value, type) {
    if (dataDecoder[type]) {
      return dataDecoder[type](value)
    }
    logger.error(`Undefined decoder type: ${type}`)
    return value
  }

  encode (value, type) {
    if (dataEncoder[type]) {
      return dataEncoder[type](value)
    }
    logger.error(`Undefined encoder type: ${type}`)
    return value.toString()
  }

  genDbFields (service, atRow) {
    const config = this.tables[service]
    const dbRow = {}
    const atFields = atRow.fields
    const atToDbMap = {}
    const dbToAtMap = config.map

    for (const dbKey in dbToAtMap) {
      atToDbMap[dbToAtMap[dbKey]] = dbKey
    }

    for (const atField in atFields) {
      const dbField = atToDbMap[atField]
      const decoder = config.decoder[dbField]
      dbRow[dbField] = atFields[atField]
      if (decoder) {
        dbRow[dbField] = this.decode(dbRow[dbField], decoder)
      }
    }
    if (atRow.id) {
      dbRow.id = atRow.id
    }
    return dbRow
  }

  genAtFields (service, row) {
    const config = this.tables[service]
    const fields = {}

    Object.keys(config.map).forEach(dbKey => {
      if (dbKey === 'atUpdatedAt' || !(dbKey in row)) {
        // timestamp is read only
        return
      }
      const atKey = config.map[dbKey]
      const value = row[dbKey]
      const type = config.decoder[dbKey]
      if (type) {
        fields[atKey] = this.encode(value, type)
      } else {
        fields[atKey] = value
      }
    })

    return fields
  }

  async uploadServiceRow (service, row) {
    const airtable = this.getAtTable(service)
    const fields = this.genAtFields(service, row)
    const id = row.id
    await new Promise((resolve, reject) => {
      airtable.update(
        [{ id, fields }],
        (err, records) => {
          if (err) {
            logger.error(`Error during upload row: ${err}`)
            reject(err)
            return
          }
          if (records.length > 0) {
            resolve(records[0])
          }
          reject(new Error('Invalid update'))
        }
      )
    })
  }

  async createServiceRow (service, row) {
    const airtable = this.getAtTable(service)
    const fields = this.genAtFields(service, row)
    return new Promise((resolve, reject) => {
      airtable.create(
        [{ fields }],
        (err, records) => {
          if (err) {
            logger.error(`Error during create row: ${err}`)
            reject(err)
            return
          }
          if (records.length > 0) {
            resolve(this.genDbFields(service, records[0]))
          }
          reject(new Error('Invalid create'))
        }
      )
    })
  }
}
