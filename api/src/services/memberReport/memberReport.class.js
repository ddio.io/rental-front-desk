const { Service } = require('feathers-sequelize')
const { NotImplemented, GeneralError } = require('@feathersjs/errors')
const _ = require('lodash')
const { logger } = require('../../logger')
const PublicHistory = require('../../utils/publicHistory')

exports.MemberReport = class MemberReport extends Service {
  setup (app) {
    this.app = app
  }

  async mapState (internalState, replaceInvalidState = true) {
    internalState = internalState || '新通報'
    let stateMappings = {}
    const ticketStateService = this.app.service('ticket-state')
    try {
      stateMappings = await ticketStateService.find({
        query: {
          internalState,
          $limit: 1,
          $sort: { atUpdatedAt: -1 }
        }
      })
    } catch (error) {
      logger.error(error)
    }
    if (stateMappings.total) {
      return stateMappings.data[0].externalState
    }
    if (replaceInvalidState) {
      return '未知 Unknown'
    }
    return null
  }

  async get (id, params) {
    const resp = await super.get(id, params)
    const report = _.pick(resp, [
      'id',
      'reportedAt',
      'buildingId',
      'roomId',
      'taskType',
      'content',
      'publicHistory'
    ])
    report.state = await this.mapState(resp.state)
    return report
  }

  async patch (id, data, params) {
    if (!data.toState || !data.msg) {
      // allow only append public history
      throw new GeneralError('Missing parameters')
    }

    const externalState = await this.mapState(data.toState, false)
    if (!externalState) {
      throw new GeneralError(`Invalid state: ${data.state}`)
    }

    const ticketService = this.app.service('ticket')
    const ticket = await ticketService.get(id)
    if (ticket.booking && data.msg) {
      // log message if someone lives here
      await this.app.service('member-chat').create({
        channelType: ticket.booking.channelType,
        channelId: ticket.booking.channelId,
        type: 'text',
        data: data.msg
      })
    }
    // log public history and update state

    const aHistory = new PublicHistory(externalState, data.msg)
    const curHistory = ticket.publicHistory || []
    return ticketService.patch(id, {
      state: data.toState,
      publicHistory: [...curHistory, aHistory]
    })
  }

  async find () {
    throw new NotImplemented()
  }

  async create (data, params) {
    throw new NotImplemented()
  }

  async update (id, data, params) {
    throw new NotImplemented()
  }

  async remove (id, params) {
    throw new NotImplemented()
  }
}
