// Initializes the `member-report` service on path `/member-report`
const { MemberReport } = require('./memberReport.class')
const createModel = require('../../models/ticket.model')
const hooks = require('./memberReport.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/member-report', new MemberReport(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('member-report')

  service.hooks(hooks)
}
