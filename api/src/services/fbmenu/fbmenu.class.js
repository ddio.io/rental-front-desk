const axios = require('axios')
const _ = require('lodash')
const { NotFound, NotAcceptable } = require('@feathersjs/errors')
const { Service } = require('feathers-sequelize')
const { logger } = require('../../logger')

const NON_MEMBER_ID = '__NONE_MEMBER__'
const PROFILE_ENDPOINT = 'https://graph.facebook.com/v7.0/me/messenger_profile'
const USER_ENDPOINT = 'https://graph.facebook.com/v7.0/me/custom_user_settings'

// TODO: manage booking from UI

exports.Fbmenu = class Fbmenu extends Service {
  setup (app) {
    this.app = app

    const bookingService = app.service('booking')

    bookingService.on('created', (booking) => {
      this.handleMemberOnboard(booking, 'created')
    })

    bookingService.on('patched', (booking) => {
      this.handleMemberOnboard(booking, 'patched')
    })
  }

  async get (id, params) {
    const row = await super.get(id, params)
    if (id === NON_MEMBER_ID) {
      return row
    }
    if (!id.match(/[a-zA-Z0-9]+/)) {
      throw new NotAcceptable()
    }
    const fbToken = this.app.get('fbAccessToken')
    const endpoint = `${USER_ENDPOINT}?psid=${id}&access_token=${fbToken}`
    const resp = await axios.get(endpoint)
    if (resp && resp.data) {
      return {
        ...row,
        menu: resp.data
      }
    }
    return row
  }

  getLastMsgTime (msgList) {
    return msgList.reduce((maxTime, msg) => {
      if (msg.atUpdatedAt > maxTime) {
        maxTime = msg.atUpdatedAt
      }
      return maxTime
    }, '')
  }

  async handleMemberOnboard (booking, method) {
    if (booking.channelType === 'facebook' && booking.channelId) {
      const menuMsgs = await this.getMemberMenu()
      if (!menuMsgs.length) {
        return
      }
      await this.createMemberMenu(menuMsgs, booking.channelId)
    }
  }

  async sync () {
    await this.syncNonMember()
    await this.syncMember()
  }

  getMemberMenu () {
    const botMsgService = this.app.service('bot-msg')
    return botMsgService.find({
      query: {
        category: '選單',
        key: {
          $ne: '非室友選單'
        },
        $sort: {
          key: 1
        }
      },
      provider: 'server',
      paginate: false
    })
  }

  async syncMember () {
    // update FB menu when it's older than bot-msg
    const menuMsgs = await this.getMemberMenu()
    if (!menuMsgs.length) {
      return
    }
    const updatedAt = this.getLastMsgTime(menuMsgs)
    const menuRows = await this.find({
      query: {
        fbid: {
          $ne: NON_MEMBER_ID
        },
        menuUpdatedAt: {
          $lt: updatedAt
        }
      },
      provider: 'server',
      paginate: false
    })
    for (const row of menuRows) {
      await this.createMemberMenu(menuMsgs, row.fbid)
    }

    // TODO: only do this when needed to avoid full table dump
    await this.checkConsistency(menuMsgs)
  }

  async checkConsistency (msgs) {
    // add FB menu for whom didn't get it during onboard
    // remove FB menu for whom has been removed from airtable
    const allBookingMembers = await this.app.service('booking').getMemberIds('facebook')
    let allMenuMembers = await this.find({
      provider: 'server',
      paginate: false
    })
    allMenuMembers = allMenuMembers.map(row => row.fbid)
    const missingMembers = _.difference(allBookingMembers, allMenuMembers)
    const removedMembers = _.difference(allMenuMembers, allBookingMembers)

    for (const member of missingMembers) {
      await this.createMemberMenu(msgs, member)
    }

    for (const member of removedMembers) {
      if (member !== NON_MEMBER_ID) {
        await this.removeMemberMenu(member)
      }
    }
  }

  async syncNonMember () {
    // update FB menu when it's older than bot-msg
    const botMsgService = this.app.service('bot-msg')
    const menuMsgs = await botMsgService.find({
      query: {
        category: '選單',
        key: '非室友選單'
      },
      provider: 'server',
      paginate: false
    })
    if (!menuMsgs.length) {
      return
    }
    const updatedAt = this.getLastMsgTime(menuMsgs)
    let menuRow = null
    try {
      menuRow = await this.get(NON_MEMBER_ID, { provider: 'server' })
    } catch (error) {
      if (error instanceof NotFound) {
        menuRow = await this.create({
          fbid: NON_MEMBER_ID
        }, { provider: 'server' })
      } else {
        throw error
      }
    }
    if (menuRow && menuRow.menuUpdatedAt < updatedAt) {
      await this.createNonMemberMenu(menuMsgs, updatedAt)
    }
  }

  async removeMemberMenu (fbid) {
    try {
      await this.get(fbid)
    } catch (error) {
      // not existed, return
      return
    }

    const fbToken = this.app.get('fbAccessToken')
    const endpoint = `${USER_ENDPOINT}?params=[%22persistent_menu%22]&access_token=${fbToken}&psid=${fbid}`
    try {
      await axios.delete(endpoint)
    } catch (error) {
      logFbError(`Failed remove user menu for user ${fbid}.`, error)
    }
    await this.remove(fbid, { provider: 'server' })
  }

  async createMemberMenu (msgs, fbid) {
    const timestamp = this.getLastMsgTime(msgs)
    const fbToken = this.app.get('fbAccessToken')
    const endpoint = `${USER_ENDPOINT}?access_token=${fbToken}`
    const enMsgs = msgs
      .filter(msg => msg.language === '英語')
      .map(msg => ({
        type: 'postback',
        title: msg.content,
        payload: msg.payload
      }))
    const zhMsgs = msgs
      .filter(msg => msg.language === '華語')
      .map(msg => ({
        type: 'postback',
        title: msg.content,
        payload: msg.payload
      }))

    if (!enMsgs.length) {
      throw new Error('Missing EN member menu config')
    }

    function genEntry (msgs, language = 'default') {
      return {
        locale: language,
        composer_input_disabled: false,
        call_to_actions: msgs
      }
    }
    const payload = {
      psid: fbid,
      persistent_menu: [genEntry(enMsgs)]
    }

    if (zhMsgs.length) {
      payload.persistent_menu.push(
        genEntry(zhMsgs, 'zh_TW'),
        genEntry(zhMsgs, 'zh_HK')
      )
    }
    try {
      await axios.post(endpoint, payload)
      logger.info(`Menu for ${fbid} is created`)
    } catch (error) {
      logFbError(`Failed create user menu for user ${fbid} - ${JSON.stringify(payload)}.`, error)
    }
    let isExisted = true
    try {
      await this.get(fbid)
    } catch (error) {
      if (error instanceof NotFound) {
        isExisted = false
      }
    }
    if (isExisted) {
      await this.patch(fbid, {
        menuUpdatedAt: timestamp
      }, { provider: 'server' })
    } else {
      await this.create({
        fbid,
        menuUpdatedAt: timestamp
      }, { provider: 'server' })
    }
  }

  async createNonMemberMenu (msgs, timestamp) {
    const fbToken = this.app.get('fbAccessToken')
    const endpoint = `${PROFILE_ENDPOINT}?access_token=${fbToken}`
    const enMsg = msgs.find(msg => msg.language === '英語')
    const zhMsg = msgs.find(msg => msg.language === '華語')
    if (!enMsg) {
      throw new Error('Missing EN non-member menu config')
    }

    function genEntry (msg, language = 'default') {
      return {
        locale: language,
        composer_input_disabled: false,
        call_to_actions: [{
          type: 'postback',
          title: msg.content,
          payload: msg.payload
        }]
      }
    }
    const payload = { persistent_menu: [genEntry(enMsg)] }

    if (zhMsg) {
      payload.persistent_menu.push(
        genEntry(zhMsg, 'zh_TW'),
        genEntry(zhMsg, 'zh_HK')
      )
    }
    // create get-started button
    try {
      await axios.post(endpoint, {
        get_started: {
          payload: 'GET_STARTED'
        }
      })
    } catch (error) {
      logFbError('Failed to set get started button.', error)
    }
    // create menu
    try {
      await axios.post(endpoint, payload)
    } catch (error) {
      logFbError(`Failed create default menu - ${JSON.stringify(payload)}.`, error)
    }
    await this.patch(NON_MEMBER_ID, {
      menuUpdatedAt: timestamp
    }, { provider: 'server' })
  }
}

function logFbError (msg, error) {
  if (error.response) {
    msg += ` Code: ${error.response.status}`
  }
  if (error.response.data.error) {
    msg += `, Msg: ${error.response.data.error.message}`
  }
  logger.error(msg)
}
