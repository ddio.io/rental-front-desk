// Initializes the `fbmenu` service on path `/fbmenu`
const { Fbmenu } = require('./fbmenu.class')
const createModel = require('../../models/fbmenu.model')
const hooks = require('./fbmenu.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/fbmenu', new Fbmenu(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('fbmenu')

  service.hooks(hooks)
}
