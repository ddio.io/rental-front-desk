const { onlyInternal } = require('../../utils/authenticate')

module.exports = {
  before: {
    all: [],
    find: [onlyInternal],
    get: [],
    create: [onlyInternal],
    update: [onlyInternal],
    patch: [onlyInternal],
    remove: [onlyInternal]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
