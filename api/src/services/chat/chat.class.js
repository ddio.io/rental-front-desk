const { Service } = require('feathers-sequelize')
const { NotImplemented, NotAuthenticated, GeneralError } = require('@feathersjs/errors')
const { getClient } = require('bottender')

const moment = require('moment')

exports.Chat = class Chat extends Service {
  constructor (options, app) {
    options.events = ['append']
    super(options, app)
    this.messenger = null
  }

  create () {
    throw new NotImplemented()
  }

  patch (id, data) {
    if (!('isRead' in data)) {
      throw new GeneralError('Invalid data field')
    }
    return super.patch(id, { isRead: data.isRead })
  }

  put () {
    throw new NotImplemented()
  }

  remove () {
    throw new NotImplemented()
  }

  find (params) {
    if (params.provider !== 'server') {
      throw new NotAuthenticated()
    }
    return super.find(params)
  }

  async get (id, params) {
    const theChat = await super.get(id, params)
    const prevChats = await this.find({
      provider: 'server',
      paginate: false,
      query: {
        channelType: theChat.channelType,
        channelId: theChat.channelId,
        date: {
          $lt: theChat.date
        },
        $sort: {
          date: -1
        },
        $limit: 1
      }
    })
    if (prevChats.length) {
      theChat.prevId = prevChats[0].id
    } else {
      theChat.prevId = null
    }
    return theChat
  }

  getMessenger () {
    if (this.messenger) {
      return this.messenger
    }
    this.messenger = getClient('messenger')
    return this.messenger
  }

  async append ({ channelType = 'facebook', channelId, chatLog }) {
    const today = moment().format('YYYY-MM-DD')
    const [targetRow, created] = await this.Model.findOrCreate({
      where: {
        date: today,
        channelType,
        channelId
      }
    })
    if (created) {
      targetRow.logs = []
    }
    targetRow.logs = [
      ...targetRow.logs,
      chatLog.valueOf()
    ]

    if (!chatLog.isFromAdmin) {
      // mark as unread if got new msg from client or system
      targetRow.isRead = false
    } else {
      // push to messenger if from admin
      // support only text for now
      this.getMessenger().sendText(
        channelId,
        chatLog.data,
        {
          tag: 'CONFIRMED_EVENT_UPDATE',
          ...(chatLog.options || {})
        }
      )
      if (!chatLog.isAutoReply) {
        targetRow.isRead = true
      }
    }

    this.emit('append', {
      channelType,
      channelId,
      chatLog
    })
    return targetRow.save()
  }
}
