// Initializes the `cannedmsg` service on path `/canned-msg`
const { CannedMsg } = require('./cannedMsg.class')
const createModel = require('../../models/cannedMsg.model')
const hooks = require('./cannedMsg.hooks')

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/canned-msg', new CannedMsg(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('canned-msg')

  service.hooks(hooks)
}
