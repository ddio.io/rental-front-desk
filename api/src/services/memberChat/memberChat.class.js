const { BadRequest, NotFound } = require('@feathersjs/errors')
const ChatLog = require('../../utils/chatlog')

exports.MemberChat = class MemberChat {
  constructor (options) {
    this.options = options || {}
  }

  setup (app) {
    this.app = app
  }

  async find (params) {
    // support only isRead for now
    const chatService = this.app.service('chat')
    const sequelize = this.app.get('sequelizeClient')

    // get last chat for each member
    const lastChatSql = 'select max(id) as id, "channelType", "channelId" from chat group by "channelType", "channelId"'
    // eslint-disable-next-line no-unused-vars
    const [results, metadata] = await sequelize.query(lastChatSql)
    const lastIds = results.map(row => row.id)

    // get chat content
    if (!params.query) {
      params.query = {}
    }
    params.query.id = { $in: lastIds }
    params.provider = 'server'
    const chats = await chatService.find(params)
    return this.correlateMeta(chats.data)
  }

  async get (id, params) {
    const chatService = this.app.service('chat')
    const chat = await chatService.get(id, params)
    if (chat) {
      const richChats = await this.correlateMeta([chat])
      return richChats[0]
    }
    throw new NotFound()
  }

  async create (data) {
    // accept message from admin only
    if (!data.channelType || !data.channelId || !data.type || !data.data) {
      throw new BadRequest('Missing Required Parameters')
    }
    const chatService = this.app.service('chat')
    const validChats = chatService.find({
      provider: 'server',
      query: {
        channelType: data.channelType,
        channelId: data.channelId,
        $sort: {
          date: -1
        },
        $limit: 1
      }
    })
    if (validChats.total <= 0) {
      throw new NotFound()
    }
    const chatLog = new ChatLog({
      type: data.type,
      data: data.data
    }, 'admin')

    return chatService.append({
      channelType: data.channelType,
      channelId: data.channelId,
      chatLog
    })
  }

  async correlateMeta (chats) {
    // correlate booking and ticket info
    const [chatsWithBooking, bookings] = await this.correlateBookings(chats)
    const [chatsWithTickets] = await this.correlateTickets(chatsWithBooking, bookings)
    return chatsWithTickets
  }

  async correlateBookings (chats) {
    const channelIds = chats.map(chat => chat.channelId)
    const bookingService = this.app.service('booking')
    const bookings = await bookingService.find({
      query: {
        channelType: 'facebook',
        channelId: { $in: channelIds },
        $sort: { updatedAt: 1 }
      }
    })
    // TODO: add relation by model.associate
    const bookingMap = {}
    bookings.data.forEach(booking => {
      // only keep members' last booking
      bookingMap[booking.channelId] = booking
    })
    const rows = chats.map(chat => {
      const booking = bookingMap[chat.channelId] || null
      return {
        ...chat,
        booking
      }
    })
    return [rows, bookings.data]
  }

  async correlateTickets (chats, bookings) {
    const memberEmails = bookings.map(booking => booking.email)
    const ticketService = this.app.service('ticket')
    const tickets = await ticketService.find({
      query: {
        reporterEmail: { $in: memberEmails }
      }
    })
    const ticketMap = {}
    tickets.data.forEach(ticket => {
      const email = ticket.reporterEmail
      if (!ticketMap[email]) {
        ticketMap[email] = []
      }
      ticketMap[email].push({
        id: ticket.id,
        taskType: ticket.taskType,
        state: ticket.state
      })
    })

    const rows = chats.map(chat => {
      const tickets = chat.booking ? ticketMap[chat.booking.email] : []
      return {
        ...chat,
        tickets: tickets || []
      }
    })
    return [rows, tickets.data]
  }
}
