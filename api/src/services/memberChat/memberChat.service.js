// Initializes the `memeberchat` service on path `/member-chat`
const { MemberChat } = require('./memberChat.class')
const hooks = require('./memberChat.hooks')

module.exports = function (app) {
  const options = {
    // paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/member-chat', new MemberChat(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('member-chat')

  service.hooks(hooks)
}
