const health = require('./health/health.service.js')
const booking = require('./booking/booking.service.js')
const chat = require('./chat/chat.service.js')
const airsync = require('./airsync/airsync.service.js')
const ticket = require('./ticket/ticket.service.js')
const cannedMsg = require('./cannedMsg/cannedMsg.service.js')
const slackMapping = require('./slackMapping/slackMapping.service.js')
const ticketState = require('./ticketState/ticketState.service.js')
const holiday = require('./holiday/holiday.service.js')
const memberChat = require('./memberChat/memberChat.service.js')
const users = require('./users/users.service.js')
const memberReport = require('./memberReport/memberReport.service.js')

const slackHook = require('./slack-hook/slack-hook.service.js')

const setting = require('./setting/setting.service.js')

const botMsg = require('./bot-msg/bot-msg.service.js')

const fbmenu = require('./fbmenu/fbmenu.service.js')

module.exports = function (app) {
  app.configure(users)
  app.configure(health)
  app.configure(booking)
  app.configure(chat)
  app.configure(airsync)
  app.configure(ticket)
  app.configure(cannedMsg)
  app.configure(slackMapping)
  app.configure(ticketState)
  app.configure(holiday)
  app.configure(memberChat)
  app.configure(memberReport)
  app.configure(slackHook)
  app.configure(setting)
  app.configure(botMsg)
  app.configure(fbmenu)
}
