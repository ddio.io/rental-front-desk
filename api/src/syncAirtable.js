
async function syncAll (airsync, logger, services) {
  if (!services || services.length === 0) {
    services = ['ALL_TABLES']
  }
  if (process.argv.length > 2) {
    services = process.argv.slice(2).filter(service => {
      return services.includes(service)
    })
  }
  const params = {
    provider: 'server'
  }
  logger.info(`Start sync airtable: ${services.join(', ')}`)
  await Promise.all(
    services.map(async (service) => {
      const resp = await airsync.create({ service }, params)
      if (resp.dataValues && resp.dataValues.status !== 'succeeded') {
        logger.error(`Failed to sync ${service}`)
      }
    })
  )
}

async function keepSyncAll (app, logger) {
  const airsync = app.service('airsync')
  const fbmenu = app.service('fbmenu')
  const syncPeriod = app.get('syncPeriodMinute') * 60 * 1000
  await syncAll(airsync, logger)
  await fbmenu.sync(true)

  setInterval(async () => {
    await syncAll(airsync, logger)
    await fbmenu.sync()
    await airsync.cleanup()
  }, syncPeriod)
}

if (require.main === module) {
  require('dotenv').config()
  const targetServices = process.argv.slice(2)
  const { app } = require('./app')
  const { logger } = require('./logger')
  const airsync = app.service('airsync')

  syncAll(airsync, logger, targetServices)
}

module.exports = {
  syncAll,
  keepSyncAll
}
