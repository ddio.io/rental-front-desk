// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const ticket = sequelizeClient.define('ticket', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    state: {
      type: DataTypes.STRING,
      defaultValue: ''
    },
    buildingId: {
      type: DataTypes.STRING
    },
    roomId: {
      type: DataTypes.STRING
    },
    taskType: {
      type: DataTypes.STRING
    },
    reporter: {
      type: DataTypes.STRING
    },
    reporterEmail: {
      type: DataTypes.STRING
    },
    content: {
      type: DataTypes.TEXT
    },
    contentAttachment: {
      type: DataTypes.JSONB
    },
    workingLog: {
      type: DataTypes.TEXT
    },
    workingLogAttachment: {
      type: DataTypes.JSONB
    },
    owner: {
      type: DataTypes.STRING
    },
    reportedAt: {
      type: DataTypes.DATE
    },
    finishedAt: {
      type: DataTypes.DATE
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    },
    publicHistory: {
      type: DataTypes.JSON,
      defaultValue: []
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  ticket.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return ticket
}
