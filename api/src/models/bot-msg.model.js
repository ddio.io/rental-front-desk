// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const botMsg = sequelizeClient.define('botmsg', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    key: { type: DataTypes.STRING },
    order: { type: DataTypes.NUMBER },
    category: { type: DataTypes.STRING },
    language: { type: DataTypes.STRING },
    content: { type: DataTypes.TEXT },
    offStartSec: { type: DataTypes.STRING },
    offEndSec: { type: DataTypes.STRING },
    isMemberOnly: { type: DataTypes.STRING },
    payload: { type: DataTypes.STRING },
    atUpdatedAt: { type: DataTypes.DATE }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  botMsg.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return botMsg
}
