// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const chat = sequelizeClient.define('chat', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    date: {
      type: DataTypes.DATEONLY
    },
    channelType: {
      type: DataTypes.STRING
    },
    channelId: {
      type: DataTypes.STRING
    },
    isRead: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    logs: {
      type: DataTypes.JSONB
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    },
    indexes: [
      {
        unique: true,
        fields: ['date', 'channelType', 'channelId']
      }
    ]
  })

  // eslint-disable-next-line no-unused-vars
  chat.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return chat
}
