// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const slackmapping = sequelizeClient.define('slackmapping', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    taskType: {
      type: DataTypes.STRING
    },
    maxReplyHour: {
      type: DataTypes.INTEGER
    },
    channel: {
      type: DataTypes.STRING
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  slackmapping.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return slackmapping
}
