// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const holiday = sequelizeClient.define('holiday', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    targetDate: {
      type: DataTypes.DATEONLY
    },
    isNotHoliday: {
      type: DataTypes.BOOLEAN
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  holiday.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return holiday
}
