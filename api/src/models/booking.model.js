// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const booking = sequelizeClient.define('booking', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    buildingId: {
      type: DataTypes.STRING
    },
    roomId: {
      type: DataTypes.STRING
    },
    floor: {
      type: DataTypes.STRING
    },
    username: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    channelType: {
      type: DataTypes.STRING
    },
    channelId: {
      type: DataTypes.STRING
    },
    channelMeta: {
      type: DataTypes.JSONB
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    },
    indexes: [
      {
        unique: false,
        fields: ['buildingId', 'roomId', 'email']
      }
    ]
  })

  // eslint-disable-next-line no-unused-vars
  booking.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return booking
}
