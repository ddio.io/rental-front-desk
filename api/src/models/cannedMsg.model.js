// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const cannedmsg = sequelizeClient.define('cannedmsg', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    state: {
      type: DataTypes.STRING
    },
    msg: {
      type: DataTypes.STRING
    },
    offStartSec: {
      type: DataTypes.INTEGER
    },
    offEndSec: {
      type: DataTypes.INTEGER
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  cannedmsg.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return cannedmsg
}
