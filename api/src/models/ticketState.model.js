// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const ticketstate = sequelizeClient.define('ticketstate', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    internalState: {
      type: DataTypes.STRING
    },
    externalState: {
      type: DataTypes.STRING
    },
    atUpdatedAt: {
      type: DataTypes.DATE
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  ticketstate.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return ticketstate
}
