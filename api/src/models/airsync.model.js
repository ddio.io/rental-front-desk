// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient')
  const airsync = sequelizeClient.define('airsync', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    service: {
      type: DataTypes.ENUM([
        'holiday',
        'ticket-state',
        'slack-mapping',
        'canned-msg',
        'booking',
        'ticket',
        'ALL_TABLES',
        'slack-hook',
        'bot-msg'
      ]),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM(['doing', 'succeeded', 'failed']),
      defaultValue: 'doing'
    },
    error: {
      type: DataTypes.STRING,
      defaultValue: ''
    }
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true
      }
    }
  })

  // eslint-disable-next-line no-unused-vars
  airsync.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return airsync
}
