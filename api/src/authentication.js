// const { AuthenticationService } = require('@feathersjs/authentication')
const { Auth0Service, Auth0Strategy } = require('@morphatic/feathers-auth0-strategy')

const SPLIT_HEADER = /(\S+)\s+(\S+)/

class MyAuth0Strategy extends Auth0Strategy {
  async parse (req) {
    const { header, schemes } = this.configuration
    const headerValue = req.headers && req.headers[header.toLowerCase()]

    if (!headerValue || typeof headerValue !== 'string') {
      return null
    }

    const [, scheme, schemeValue] = headerValue.match(SPLIT_HEADER) || []
    const hasScheme = scheme && schemes.some(
      current => new RegExp(current, 'i').test(scheme)
    )

    if (scheme && !hasScheme) {
      return null
    }

    return {
      strategy: this.name,
      accessToken: hasScheme ? schemeValue : headerValue
    }
  }
}

module.exports = app => {
  const authentication = new Auth0Service(app)

  authentication.register('auth0', new MyAuth0Strategy())

  app.use('/authentication', authentication)
}
